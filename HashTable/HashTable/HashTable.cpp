#include <iostream>
#include <string>
#include <cmath>
#include <sstream>

struct KeyValuePair {
    std::string key;
    int value;
    KeyValuePair* next;

    KeyValuePair(const std::string& k, int v) : key(k), value(v), next(nullptr) {}
};

class HashTable {
private:
    static const int INIT_SIZE = 4;
    KeyValuePair** table;
    int currentSize;
    int maxItems;

    long int GenerateHash(const std::string& key) {
        long int hash = 0;
        for (int i = 1; i <= key.length(); i++) {
            hash += (key[i - 1] * pow(31, key.length() - i));
        }
        return hash;
    }

    int GetIndexFromHash(long int hash, int size) {
        return abs(hash % size);
    }

    void NullifyTable(KeyValuePair** table, int size) {
        for (int i = 0; i < size; ++i) {
            table[i] = nullptr;
        }
    }

    void Rehash() {
        int newSize = maxItems * 2;
        KeyValuePair** newTable = new KeyValuePair*[newSize];

        NullifyTable(newTable, newSize);

        for (int i = 0; i < maxItems; ++i) {
            KeyValuePair* current = table[i];
            while (current != nullptr) {
                KeyValuePair* temp = current->next;
                long int newIndex = GetIndexFromHash(GenerateHash(current->key), newSize);
                current->next = newTable[newIndex];
                newTable[newIndex] = current;
                current = temp;
            }
        }

        delete[] table;
        table = newTable;
        maxItems = newSize;
    }


public:
    HashTable() : currentSize(0), maxItems(INIT_SIZE) {
        table = new KeyValuePair*[maxItems];
        NullifyTable(table, maxItems);
    }

    ~HashTable() {
        Clear();
    }

    void Add(const std::string& key, int value) {
        long int index = GetIndexFromHash(GenerateHash(key), maxItems);
        KeyValuePair* existingKeyValuePair = Search(key);
        if (existingKeyValuePair != nullptr) {
            existingKeyValuePair->value = value;
            return;
        }
        KeyValuePair* entry = new KeyValuePair(key, value);

        if (table[index] == nullptr) {
            table[index] = entry;
        }
        else {
            KeyValuePair* current = table[index];
            while (current->next != nullptr) {
                current = current->next;
            }
            current->next = entry;
        }

        currentSize++;

        if (currentSize >= 0.75 * maxItems) {
            Rehash();
        }
    }

    KeyValuePair* Search(const std::string& key) {
        long int index = GetIndexFromHash(GenerateHash(key), maxItems);
        KeyValuePair* current = table[index];

        while (current != nullptr) {
            if (current->key == key) {
                return current;
            }
            current = current->next;
        }

        return nullptr;
    }

    bool Remove(const std::string& key) {
        long int index = GetIndexFromHash(GenerateHash(key), maxItems);
        KeyValuePair* current = table[index];
        KeyValuePair* prev = nullptr;

        while (current != nullptr) {
            if (current->key == key) {
                if (prev == nullptr) {
                    table[index] = current->next;
                }
                else {
                    prev->next = current->next;
                }
                delete current;
                currentSize--;
                return true;
            }
            prev = current;
            current = current->next;
        }

        return false;
    }

    void Clear() {
        if (currentSize > 0) {
            for (int i = 0; i < maxItems; ++i) {
                KeyValuePair* current = table[i];
                while (current != nullptr) {
                    KeyValuePair* temp = current;
                    current = current->next;
                    delete temp;
                }
            }
            delete[] table;
            currentSize = 0;
            maxItems = INIT_SIZE;
        }
    }

    std::string ToString() {
        std::string distancer = "  ";
        std::stringstream ss;
        ss << "Hash table:\n";
        ss << distancer << "Current size: " << currentSize << "\n";
        ss << distancer << "Max size: " << maxItems << "\n";
        
        if (currentSize > 0) {
            ss << distancer << "{\n";

            for (int i = 0; i < maxItems; ++i) {
                KeyValuePair* current = table[i];

                if (current != nullptr) {
                    ss << distancer << distancer << i << ": ";

                    while (current != nullptr) {
                        ss << current->key << " -> " << current->value << "; ";
                        current = current->next;
                    }
                    ss << "\n";
                }
            }

            ss << distancer << "}\n";
        }

        return ss.str();
    }
};

std::string generateKey(int length = 5) {
    std::string key = "";
    const std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (int i = 0; i < length; ++i) {
        key += alphabet[rand() % alphabet.length()];
    }

    return key;
}

int main() {
    HashTable hashTable;

    for (int i = 1; i <= 100; i++)
    {
        hashTable.Add(generateKey(), i);
    }
    std::cout << hashTable.ToString() << "\n";

    std::string testKey = "asdfg";
    hashTable.Add(testKey, 7);
    hashTable.Add(testKey, 88);
    std::cout << hashTable.ToString() << "\n";

    KeyValuePair* foundKeyValuePair = hashTable.Search(testKey);
    std::cout << "For key '" << testKey << "' found value:" << foundKeyValuePair->value << "\n\n\n";

    bool res1 = hashTable.Remove(testKey);
    bool res2 = hashTable.Remove(testKey);
    std::cout << "Result of first remove: " << res1 << ", result of second remove: " << res2 << ".\n\n";
    std::cout << hashTable.ToString() << "\n";

    hashTable.Clear();
    std::cout << hashTable.ToString() << "\n";

    return 0;
}

